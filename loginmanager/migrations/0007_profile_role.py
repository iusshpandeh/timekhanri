# Generated by Django 3.1 on 2020-08-28 09:28

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('loginmanager', '0006_profile_monthly_pay'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='Role',
            field=models.CharField(blank=True, help_text='Role', max_length=200, null=True),
        ),
    ]
