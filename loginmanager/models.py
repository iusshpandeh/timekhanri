from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _

# Create your models here.
gender_choice = (
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'),
)

class Profile(models.Model):
    user = models.OneToOneField(User, help_text=_('User'), on_delete=models.CASCADE, null=True)
    first_name = models.CharField(help_text=_('first name'), max_length=200, null=True, blank=True)
    last_name = models.CharField(help_text=_('last name'), max_length=200, null=True, blank=True)
    email = models.CharField(help_text=_('Email'), max_length=200, null=True, blank=True)
    phone = models.CharField(help_text=_('Phone'), max_length=200, null=True, blank=True)
    role = models.CharField(help_text=_('Role'), max_length=200, null=True, blank=True)
    gender = models.CharField(help_text=_('Gender'), max_length=10, choices=gender_choice, null=True, blank=True)
    address = models.CharField(help_text=_('Address'), max_length=200, null=True, blank=True)
    bank_name = models.CharField(help_text=_('Bank name'), max_length=200, null=True, blank=True)
    bank_account = models.CharField(help_text=_('Bank account'), max_length=200, null=True, blank=True)
    rate_per_hour = models.FloatField(help_text=_('Rate Per Hour'), null=True, blank=True)
    monthly_pay = models.FloatField(help_text=_('Monthly Pay'), null=True, blank=True)
    profile_pic = models.FileField(blank=True, upload_to="user_images/")

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profile')

    def __str__(self):
        return self.first_name


class Company(models.Model):
    company_name = models.CharField(help_text=_('Name'),max_length=100)
    company_address = models.CharField(help_text=_('Company Address'),max_length=250)
    company_logo = models.FileField(help_text=_('Logo'),blank=True,upload_to="company_images/")
    company_email = models.EmailField(help_text=_('Email'),max_length=254,default="")
    company_contact = models.CharField(help_text=_('Phone'),max_length=20,default="")

    def __str__(self):
        return self.company_name
