from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import Group
from django.contrib import messages
from .decorators import unauthenticated_user, admin_only
from .forms import CreateUserForm, ProfileForm, CompanyDetailsForm, ProfileDetailsForm
from django.contrib.auth.decorators import login_required
from .models import *
from django.contrib.auth.decorators import user_passes_test
from message.models import message
from timecard.models import DailyLog

# Create your views here.

@unauthenticated_user
def loginpage(request):
    # if request.user.is_authenticated:
    #     return redirect('home')
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('home')
        else:
            messages.info(request, 'Wrong Username or Password')

    context = {}
    return render(request, 'loginmanager/login.html', context)
    # return HttpResponse('Login home page')

def logoutpage(request):
    logout(request)
    return redirect('home')

@unauthenticated_user
def registerpage(request):
    # if request.user.is_authenticated:
    #     return redirect('home')

    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get('username')

            group = Group.objects.get(name='User')
            user.groups.add(group)

            messages.success(request, 'Account was Created for ' + username)
            return redirect('home')

    context = {'form': form}
    return render(request, 'loginmanager/register.html', context)

def forgotpasswordpage(request):
    return render(request, 'loginmanager/password.html')

def error401(request):
    return render(request, 'loginmanager/401.html')

def error404(request):
    return render(request, 'loginmanager/404.html')

def error500(request):
    return render(request, 'loginmanager/500.html')

@login_required(login_url='login')
def profile(request):
    user = request.user

    Profilefound = None
    try:
        Profilefound = request.user.profile.user
    except:
        pass

    print(Profilefound)
    if Profilefound is None:
        # print('No profile found')
        form = ProfileForm(instance=user)
    else:
        # print('Profile found')
        profile = request.user.profile
        form = ProfileForm(instance=profile)

    if request.method == 'POST':
        form = ProfileForm(request.POST)
        if Profilefound == None:
            if form.is_valid():
                profiledata = form.save(commit=False)
                profiledata.user = request.user
                # print('form is valid')
                profiledata.save()
                # form.save()
                messages.success(request, 'Profile Created Successfully ')
        else:
            form = ProfileForm(request.POST, instance=profile)
            if form.is_valid():
                form.save()
                messages.success(request, 'Profile Updated Successfully ')


    context = {'form':form}
    return render(request, 'loginmanager/profile.html', context)


@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def company_details(request):
    form = CompanyDetailsForm()
    try:
        company = Company.objects.get(id=1)
    except:
        return render(request,"loginmanager/company_not_found.html")
    if request.method == 'POST':
        form = CompanyDetailsForm(request.POST,request.FILES,instance=company)
        if form.is_valid():
            form.save()
    context = {'form':form,'company':company}
    return render(request, "loginmanager/company_details.html",context)

@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def all_user_profiles(request):
    employees = Profile.objects.filter()
    context = {"employees":employees}
    return render(request,"loginmanager/all_users.html",context)

@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def profile_details(request, emp_id):
    
    profile= Profile.objects.get(id=emp_id)

    if request.method == 'POST':
        form = ProfileDetailsForm(request.POST,request.FILES,instance=profile)
        if form.is_valid():
            form.save()


    form = ProfileDetailsForm(initial={"first_name":profile.first_name,"last_name":profile.last_name,"phone":profile.phone,"role":profile.role,"gender":profile.gender,"address":profile.address,"bank_name":profile.bank_name,"bank_account":profile.bank_account,
                                "rate_per_hour":profile.rate_per_hour,"monthly_pay":profile.monthly_pay}
    )
    context = {'form':form,'profile':profile}
    return render(request, "loginmanager/profile_details.html",context)

@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def delete_employee_confirm(request, emp_id):
    profile = Profile.objects.get(id=emp_id)
    context = {"profile":profile}
    return render(request, "loginmanager/delete_employee_confirm.html", context)
    
@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def delete_employee(request, emp_id):
     if request.method == 'POST':
        profile = Profile.objects.get(id=emp_id)
        user_name = profile.user
        user_to_deactivate = User.objects.get(username=user_name)
        message_to_delete = message.objects.all().filter(user=user_name)
        message_to_delete.delete()
        dl_to_delete = DailyLog.objects.all().filter(user=user_name)
        dl_to_delete.delete()
        print(user_to_deactivate)
        user_to_deactivate.is_active = False
        user_to_deactivate.save()
        profile.delete()
        return redirect("all_user_profiles")