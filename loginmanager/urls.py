from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('', views.loginpage, name='loginmanager-login'),
    path('register/', views.registerpage, name='loginmanager-register'),
    path('profile/', views.profile, name='profile'),
    path('logout', views.logoutpage, name='logout'),
    path('password/', views.forgotpasswordpage, name='loginmanager-password'),
    path('error401/', views.error401, name='loginmanager-401'),
    path('error404/', views.error404, name='loginmanager-404'),
    path('error500/', views.error500, name='loginmanager-500'),
    path(
        "password-reset/",
        auth_views.PasswordResetView.as_view(
            template_name="loginmanager/password_reset.html"),
        name="password_reset",
    ),
    path(
        "password-reset/done/",
        auth_views.PasswordResetDoneView.as_view(
            template_name="loginmanager/password_reset_done.html"),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(
            template_name="loginmanager/password_reset_confirm.html"),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="loginmanager/password_reset_complete.html"),
        name="password_reset_complete",
    ),
    path("company_details/",views.company_details,name="company_details"),
    path("all_user_profiles/",views.all_user_profiles, name = "all_user_profiles"),
    path("profile_details/<int:emp_id>/", views.profile_details, name="profile_details"),
    path("delete_employee_confirm/<int:emp_id>",views.delete_employee_confirm, name="delete_employee_confirm"),
    path("delete_employee/<int:emp_id>",views.delete_employee, name="delete_employee"),
]
