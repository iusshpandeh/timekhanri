from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import MessageForm
from .models import message
from .filters import MessageFilter
from django.contrib.auth.decorators import user_passes_test
# Create your views here.

def home(request):
    form = MessageForm()
    message_list = message.objects.all().filter(user_id=request.user.id).order_by('-date_created')
    context = {'form': form, 'message_list': message_list}

    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            form.save()

    return render(request, 'message/index.html', context)

@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def list_all_messages(request):
    messages = message.objects.all()
    messageFilter = MessageFilter(request.GET,queryset=messages)
    messages = messageFilter.qs
    context = {'messages': messages, 'messageFilter':messageFilter}
    return render(request, 'message/list_all_messages.html', context)

@user_passes_test(lambda u:u.is_staff, login_url='loginmanager-login')
def acknowledge_message(request,msg_id):
    message_to_ack = message.objects.get(id=msg_id)
    context = {"message_to_ack":message_to_ack}

    if request.method == 'POST':
        ack_msg = request.POST.get('ack_message')
        message_to_ack.acknowledge = ack_msg
        message_to_ack.is_acked = True
        message_to_ack.save()
        return redirect("list_all_messages")
        


    return render(request,'message/acknowledge_message.html',context)