import django_filters
from django_filters import DateFilter
from .models import *
from django.forms import DateInput
from django import forms
from django.utils.translation import gettext as _ 

class MessageFilter(django_filters.FilterSet):
    start_date = DateFilter(field_name="date_created", lookup_expr='gte',widget=DateInput(attrs={'type': 'date'}),label=_("Select Date"))
    end_date = DateFilter(field_name="date_created", lookup_expr='lte',widget=DateInput(attrs={'type': 'date'}),label="~")

    class Meta:
        model = message
        fields = '__all__'
        exclude = ['message','date_created','acknowledge']