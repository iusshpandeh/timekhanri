from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='message'),
    path('list_all_messages/',views.list_all_messages, name="list_all_messages"),
    path('acknowledge_message/<int:msg_id>/',views.acknowledge_message, name = "acknowledge_message"),
]
