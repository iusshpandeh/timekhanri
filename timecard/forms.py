from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from django.forms import DateTimeInput
from .models import *
from django.utils.translation import gettext as _

class DailyLogForm(ModelForm):
    class Meta:
        model = DailyLog
        fields = '__all__'

class ChangeTimeCardForm(ModelForm):
    class Meta:
        model = DailyLog
        fields = ["checkin_time", "checkout_time"]
    