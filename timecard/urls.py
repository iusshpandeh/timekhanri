from django.urls import path
from . import views

urlpatterns = [
    path('', views.timecard, name='checkin'),
    path('record/', views.record, name='record'),
    path('allrecord/', views.allrecord, name='allrecord'),
    path('report/', views.report, name='report'),
    path('staff_report/<int:emp_id>',views.staff_report, name="staff_report"),
    path('get_duration_graph_data/',views.get_duration_graph_data, name = "get_duration_graph_data"),
    path('edit_timecard/<int:tid>/',views.edit_timecard, name = "edit_timecard"),
]
