import django_filters
from django_filters import DateFilter, CharFilter
from .models import *
from django.forms import DateInput
from django import forms
from django.utils.translation import gettext as _

class ReportFilter(django_filters.FilterSet):
    start_date = DateFilter(field_name="checkin_time", lookup_expr='gte',widget=DateInput(attrs={'type': 'date','placeholder':'MM/DD/YYYY'}),label=(""))
    end_date = DateFilter(field_name="checkin_time", lookup_expr='lte',widget=DateInput(attrs={'type': 'date','placeholder':'MM/DD/YYYY'}),label="~")
    class Meta:
        model = DailyLog
        fields = '__all__'
        exclude = ['user','checkin_time', 'checkout_time', 'checkin_message', 'checkout_message', 'status']

class RecordFilter(django_filters.FilterSet):
    start_date = DateFilter(field_name="checkin_time", lookup_expr='gte',widget=DateInput(attrs={'type': 'date','placeholder':'MM/DD/YYYY'}),label="")
    end_date = DateFilter(field_name="checkin_time", lookup_expr='lte',widget=DateInput(attrs={'type': 'date','placeholder':'MM/DD/YYYY'}),label="~")
    
    class Meta:
        model = DailyLog
        fields = '__all__'
        exclude = ['user','checkin_time', 'checkout_time', 'checkin_message', 'checkout_message', 'status']