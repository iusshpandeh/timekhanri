from django.shortcuts import render, redirect
import csv
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.models import User
from datetime import datetime
from .forms import DailyLogForm, ChangeTimeCardForm
from .models import DailyLog
from loginmanager.models import *
from .filters import ReportFilter, RecordFilter
from django.utils.translation import gettext as _
from loginmanager.decorators import allowed_users
from django.contrib.auth.decorators import user_passes_test
from loginmanager.models import Profile
# Create your views here.

def timecard(request):
    checkin_check = DailyLog.objects.filter(user_id=request.user.id).last()
    if checkin_check == None:
        checkin_status = 0
        checkin_time = 'You have not checked In'
    else:
        data = DailyLog.objects.filter(user_id=request.user.id).last()
        pk_id = data.id
        checkin_status = data.status
        checkin_time = data.checkin_time
        checkin = DailyLog.objects.get(id=pk_id)

    name = request.user
    context = {'name': name, 'checkin_status': checkin_status, 'checkin_time': checkin_time}
    print(checkin_status)
    if request.method == 'POST':
        if 'checkin' in request.POST:
            form = DailyLogForm(request.POST)
            if form.is_valid():
                form.save()
                return redirect('checkin')
                # return render(request, 'timecard/index.html', context)

        elif 'checkout' in request.POST:
            checkin.checkout_message = request.POST['checkout_message']
            checkin.status = 0
            checkin.checkout_time = datetime.now()
            checkin.save()
            # form = DailyLogForm(instance=checkin)
            # form = DailyLogForm(request.POST, instance=checkin)
            # if form.is_valid():
            #     form.save(["checkout_message"])
            return redirect('checkin')

    print(checkin_status)
    return render(request, 'timecard/index.html', context)

def record(request):
    pk_id = request.user.id
    records = DailyLog.objects.all().filter(user_id=pk_id)
    recordFilter = RecordFilter(request.GET, queryset = records)
    records = recordFilter.qs
    context = {'records': records,'recordFilter':recordFilter}
    return render(request, 'timecard/record.html', context)
    
@user_passes_test(lambda u:u.is_staff, login_url=None)
def allrecord(request):
    pk_id = request.user.id
    records = DailyLog.objects.all()
    context = {'records': records}
    return render(request, 'timecard/allrecord.html', context)

def report(request):
    records = DailyLog.objects.all()
    myFilter = ReportFilter(request.GET, queryset=records)
    records = myFilter.qs
    context = {'records': records, 'myFilter': myFilter}
    return render(request, 'timecard/report.html', context)

""" @user_passes_test(lambda u:u.is_staff, login_url=None)
def getreport(request):
    records = DailyLog.objects.all()
    myFilter = ReportFilter(request.GET, queryset=records)
    records = myFilter.qs
    context = {'records': records, 'myFilter': myFilter}
    if request.method == 'GET':
        if 'download' in request.GET:
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="file.csv"'
            writer = csv.writer(response)
            writer.writerow([_('Username'), _('CheckIn Time'), _('CheckIN Message'), _('CheckOut Time'), _('CheckOut Message'), _('Duration'), _('Payment')])
            writer.writerow([''])
            total = 0
            rate = 0
            for r in records:
                try:
                    rates = Profile.objects.get(user_id=r.user_id)
                    if rates.rate_per_hour == None:
                        rate = 0
                    else:
                        rate = rates.rate_per_hour
                except:
                    pass

                duration = r.checkout_time - r.checkin_time
                # payment = duration
                days, seconds = duration.days, duration.seconds
                hours = days * 24 + seconds // 3600
                minutes = (seconds % 3600) // 60
                seconds = seconds % 60
                payment = (hours * rate) + (minutes * rate/60) + (seconds * rate/3600)
                total = total + payment
                writer.writerow([r.user.username, r.checkin_time, r.checkin_message, r.checkout_time, r.checkout_message, duration, payment])
                # writer.writerow([days, hours, minutes, seconds])
            writer.writerow([''])
            writer.writerow([_('Total'), '', '', '', '', '', total])
            return response
    return render(request, 'timecard/getreport.html', context) """


@user_passes_test(lambda u:u.is_staff, login_url=None)
def staff_report(request, emp_id):
    user_profile = Profile.objects.get(id=emp_id)
    user_name = user_profile.user
    records = DailyLog.objects.filter(user=user_name)
    myFilter = ReportFilter(request.GET, queryset=records)
    records = myFilter.qs
    context = {"records":records, "employee":user_profile, 'reportFilter':myFilter}
    if request.method == 'GET':
        if 'download' in request.GET:
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="file.csv; charset=utf-8"'
            writer = csv.writer(response)
            writer.writerow([_('Username'), _('CheckIn Time'), _('CheckIN Message'), _('CheckOut Time'), _('CheckOut Message'), _('Duration'), _('Payment')])
            writer.writerow([''])
            total = 0
            rate = 0
            total_duration = 0
            for r in records:
                try:
                    rates = Profile.objects.get(user_id=r.user_id)
                    if rates.rate_per_hour == None:
                        rate = 0
                    else:
                        rate = rates.rate_per_hour
                except:
                    pass

                duration = r.checkout_time - r.checkin_time
                # payment = duration
                days, seconds = duration.days, duration.seconds
                total_duration = total_duration + days * 24 * 60 * 60 + seconds 
                hours = days * 24 + seconds // 3600
                minutes = (seconds % 3600) // 60
                seconds = seconds % 60
                payment = (hours * rate) + (minutes * rate/60) + (seconds * rate/3600)
                total = total + payment
                writer.writerow([r.user.username, r.checkin_time, r.checkin_message, r.checkout_time, r.checkout_message, duration, payment])
                # writer.writerow([days, hours, minutes, seconds])
            total_hours = total_duration // 3600 
            total_minutes = (total_duration % 3600) // 60
            writer.writerow([''])
            writer.writerow([_('Total'), '', '', '', '', str(total_hours) + ":" + str(total_minutes), total])
            return response
    return render(request,"timecard/staff_record.html",context)

def get_duration_graph_data(request):
    records = DailyLog.objects.filter(user_id=request.user.id).order_by('-checkout_time')[:][::-1]
   
    days = []
    durations = []
    for record in records:
        duration = (record.checkout_time - record.checkin_time)
        duration_minutes = int(duration.total_seconds() / 60)
        durations.append(duration_minutes)
        day = str(record.checkout_time.day) + " " + str((record.checkout_time).strftime('%b'))
        days.append(day)

    data = {
        "days":days,
        "duration":durations,
    }
    return JsonResponse(data)

def edit_timecard(request, tid):
    timecard = DailyLog.objects.get(id=tid)
    if request.method == 'POST':
        form = ChangeTimeCardForm(request.POST,request.FILES,instance=timecard)
        if form.is_valid():
            form.save()
    form = ChangeTimeCardForm(initial={"checkin_time":timecard.checkin_time,"checkout_time":timecard.checkout_time}
    )
    context = {"form":form,"timecard":timecard}
    return render(request, "timecard/edit_timecard.html", context)