from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from message.models import message
from django.contrib.auth.decorators import login_required
from .decorators import allowed_users, admin_only
from loginmanager.models import *
from django.utils.translation import gettext as _
# Create your views here.

@login_required(login_url='loginmanager-login')
# @allowed_users(allowed_roles=['Admin', 'Subadmin'])
@admin_only
def home(request):
    # return HttpResponse('This is home page')
    if request.user.is_staff:
        return redirect("all_user_profiles")
    userinfo = Profile.objects.filter(user_id=request.user.id)
    message_list = message.objects.all().filter(user_id=request.user.id).order_by('-date_created')[:5]
    context = {'userinfo': userinfo,"message_list":message_list}
    return render(request, 'home/index.html', context)

def userPage(request):
    # return HttpResponse('This is home page')
    context = {
        'hello': _('Hello')
    }
    return render(request, 'home/user.html', context)
