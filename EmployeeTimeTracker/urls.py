from django.contrib import admin
from django.urls import path, include
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import gettext_lazy as _
import loginmanager.views as login_views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
]
urlpatterns += i18n_patterns(
    
    path(_('timekhanri_superadmin_login/'), admin.site.urls),
    path('', include('home.urls')),
    path('login/', include('loginmanager.urls')),
    path('message/', include('message.urls')),
    path('timecard/', include('timecard.urls')),
    path('test/', include('test.urls')),
    

)
# urlpatterns = [
#     path('i18n/', include('django.conf.urls.i18n')),
#     path('admin/', admin.site.urls),
#     path('', include('home.urls')),
#     path('login/', include('loginmanager.urls')),
#     path('message/', include('message.urls')),
#     path('timecard/', include('timecard.urls')),
#     path('test/', include('test.urls')),
# ]
# urlpatterns = [
#     path('admin/', admin.site.urls),
#     path('', include('home.urls')),
#     path('login/', include('loginmanager.urls')),
#     path('message/', include('message.urls')),
#     path('timecard/', include('timecard.urls')),
# ]
# urlpatterns += i18n_patterns(
#     path('test/', include('test.urls')),
# )


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)